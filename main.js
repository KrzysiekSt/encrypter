const { app, BrowserWindow, ipcMain} = require('electron');
const path = require("path");
const serverActions = require("./scripts/serverActions");
const clientActions = require("./scripts/clientActions");
const serverEventEmitter = serverActions.serverEventEmitter;
const clientEventEmitter = clientActions.clientEventEmitter;
let mainWindow;

ipcMain.on('connect', (event, address) => {
    clientActions.connect(address);

    ipcMain.on('send_new_message', (event, message) => {
        clientEventEmitter.emit('send', message);
    });
});

serverEventEmitter.on('close', () =>
{
    ipcMain.removeListener("send_new_message", serverSendMessage);
});

serverEventEmitter.on('show_chat', () =>{
    createChat("Server");
});

clientEventEmitter.on('show_chat', () =>{
    createChat("Client");
});

function createChat(title)
{
    const chatWindow = new BrowserWindow({
        width: 400,
        height: 600,
        webPreferences: {
            preload: path.join(__dirname, 'scripts/preloads/chatPreload.js'),
            nodeIntegration: true,
            contextIsolation: false,
            webSecurity: false,
        },
        parent: mainWindow,
        title: title
    })

    chatWindow.loadFile(path.join(__dirname, "./view/index.html"));
    chatWindow.webContents.openDevTools({ mode: 'detach' });

    if (title === "Server")
    {
        serverEventEmitter.on("show_message", function(message) {
            chatWindow.webContents.send('show_message', message);
        })
    }

    if (title === "Client")
    {
        clientEventEmitter.on("show_message", function (message) {
            chatWindow.webContents.send('show_message', message);
        })
    }
}

app.whenReady().then(() => {
    createWindow();
})

function createWindow()
{

    mainWindow = new BrowserWindow({
        width: 300,
        height: 200,
        webPreferences: {
            preload: path.join(__dirname, 'scripts/preloads/connectionPreload.js'),
            nodeIntegration: true,
            contextIsolation: false,
            webSecurity: false,
        },
    })

    mainWindow.loadFile('view/connection.html');
    serverActions.startServer();
    ipcMain.on('send_new_message', serverSendMessage);
}

function serverSendMessage(event, message)
{
    serverEventEmitter.emit('send', message);
}
