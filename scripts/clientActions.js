const net = require('net');
const EventEmitter = require("events");
const {SERVER_PORT, serverEventEmitter} = require("./serverActions");
const {ServerSecurity: SeverSecurityActions} = require("./security");
const ClientSecurityActions = require("./security").ClientSecurity;


const eventEmitter = new EventEmitter();
const securityActions = new ClientSecurityActions(eventEmitter);
let messageCounter = 0;
let serverPublicKey = null;
function connect(host)
{
    console.log(`connecting to ${host}:${SERVER_PORT}`)
    const client = net.createConnection({
        host: host,
        port: SERVER_PORT,
    });
    setupClientEvents(client);
    stopServer(host);
}

function setupClientEvents(client)
{
    eventEmitter.on('send', (data) =>
    {
        let decryptedMessage = securityActions.encryptMessage(data);
        client.write(decryptedMessage);
    });

    eventEmitter.on('finish_handshake', (publicKey) =>
    {
        client.write(publicKey);
    });

    client.on('connect', () => {
        console.log('Connected to the server');
        eventEmitter.emit('show_chat');
    });

    client.on('data', (data) => {
        if (serverPublicKey === null)
        {
            serverPublicKey = data;
            securityActions.confirmNegotiation(serverPublicKey);
        }
        else
        {
            let message = securityActions.decryptMessage(data);
            eventEmitter.emit('show_message', message);
        }
    });

    client.on('close', () => {
        console.log('Connection closed');
    });

    client.on('error', (err) => {
        console.error('Error:', err);
    });
}

function stopServer(host)
{
    if(host === "127.0.0.1")
    {
        return;
    }
    serverEventEmitter.emit('close');
}


module.exports = {
    connect,
    clientEventEmitter: eventEmitter
};