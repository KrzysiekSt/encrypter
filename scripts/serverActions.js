const net = require('net');
const EventEmitter = require("events");
const SeverSecurityActions = require("./security").ServerSecurity;

const eventEmitter = new EventEmitter();
const securityActions = new SeverSecurityActions(eventEmitter);
const SERVER_PORT = 34854;
let server;
let messageCounter = 0;
let clientPublicKey = null;

function startServer()
{
    server = net.createServer();
    setupServerEvents();
}

function setupServerEvents()
{
    server.on('connection', (socket) => {
        console.log(`New client connected: ${socket.address().toString()}`);

        eventEmitter.on('start_handshake', (publicKeys) =>
        {
            socket.write(publicKeys);
        });

        securityActions.initNegotiation();

        eventEmitter.emit('show_chat');

        eventEmitter.on('send', (data) =>
        {
            let encryptedMessage = securityActions.encryptMessage(data);
            socket.write(encryptedMessage);
        });

        eventEmitter.on('close', () =>
        {
            server.close();
        });

        socket.on('data', (data) =>
        {
            if (clientPublicKey === null)
            {
                clientPublicKey = data;
                securityActions.finishDiffieHellmanNegotiation(clientPublicKey);
            }
            else
            {
                let message = securityActions.decryptMessage(data);
                eventEmitter.emit('show_message', message);
            }
        });

        socket.on('close', () =>
        {
            console.log('Client disconnected');
        });
    });

    server.on('error', (error) =>
    {
        console.log(`name: ${error.name} \n message: ${error.message}`);
    });

    server.listen(SERVER_PORT, () =>
    {
        console.log(`Server listening on port ${SERVER_PORT}`);
    });
}





module.exports =
{
    startServer,
    serverEventEmitter: eventEmitter,
    SERVER_PORT
};