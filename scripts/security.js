const crypto = require("crypto");

class Security
{
    _eventEmitter;
    _diffieHellmanObject;
    _AES_KEY;
    IV_SIZE_IN_BYTES = 8;
    IV_SIZE_IN_HEX = 16;
    HASH_SIZE = 512;
    SIGNATURE_SIZE = 512;
    _rsaMyPrivateKey = null;
    _rsaOtherPublicKey = null;


    constructor(object)
    {
        this._eventEmitter = object;
    }

    encryptMessage(data)
    {
        const iv = crypto.randomBytes(this.IV_SIZE_IN_BYTES).toString('hex');

        let encryptedMessageValue = this.encryptData(iv, data);

        const calculatedHash = this.calculateHash(encryptedMessageValue);

        const encryptedHash = crypto.publicEncrypt(this._rsaOtherPublicKey, Buffer.from(calculatedHash, 'hex')).toString('hex');
        console.log('encrypted hash:', calculatedHash);

        const signer = crypto.createSign('RSA-SHA256');
        signer.update(calculatedHash);
        const signature = signer.sign(this._rsaMyPrivateKey, 'hex');

        // console.log(`---------------- send buffer content ----------------\n
        //     iv: \n ${iv} \n
        //     hash: \n ${calculatedHash} \n
        //     signature: \n ${signature} \n
        //     data: \n ${encryptedMessageValue} \n
        // `);

        const  MESSAGE_BUFFER = Buffer.from(iv + encryptedHash + signature + encryptedMessageValue, 'hex');

        return MESSAGE_BUFFER;
    }

    encryptData(iv, data)
    {
        const cipher = crypto.createCipheriv('aes-256-cbc', this._AES_KEY.toString('hex'), iv.toString('hex'));
        let encryptedData = cipher.update(data, 'utf8', 'hex');
        encryptedData += cipher.final('hex');

        // console.log(`---------------- Got buffer content ----------------\n
        //     AES: \n ${this._AES_KEY.toString('hex')} \n
        // `);

        return encryptedData;
    }

    calculateHash(data)
    {
        const hashObject = crypto.createHash('sha256');
        hashObject.update(data);
        const calculatedHash = hashObject.digest('hex');

        return calculatedHash;
    }

    decryptMessage(message_buffer)
    {
        const messageHex = message_buffer.toString('hex');
        let message = this.splitMessageData(messageHex);

        try
        {
            const decryptedHash = this.verifyHash(message.messageValue, message.hash);
            this.verifySignature(decryptedHash, message.signature);
            const DECRYPTED_MESSAGE_VALUE = this.decryptData(message.iv, message.messageValue);

            return DECRYPTED_MESSAGE_VALUE;
        }
        catch (error)
        {
            console.error('An error occurred:', error.message);
            return `Message corrupted: ${error.message}`;
        }
    }

    splitMessageData(messageStringHex)
    {
        const ivStringEnd = this.IV_SIZE_IN_HEX;
        const hashStringEnd = this.IV_SIZE_IN_HEX + this.HASH_SIZE;
        const signatureStringEnd = this.IV_SIZE_IN_HEX + this.HASH_SIZE + this.SIGNATURE_SIZE;

        const iv = messageStringHex.slice(0, ivStringEnd);
        const hash = messageStringHex.slice(ivStringEnd, hashStringEnd);
        const signature = messageStringHex.slice(hashStringEnd, signatureStringEnd);
        const messageValue = messageStringHex.slice(signatureStringEnd);

        // console.log(`---------------- Got buffer content ----------------\n
        //     iv: \n ${iv} \n
        //     hash: \n ${hash} \n
        //     signature: \n ${signature} \n
        //     messageValue: \n ${messageValue} \n
        // `);

        return {
            iv: iv,
            hash: hash,
            signature: signature,
            messageValue: messageValue
        }
    }

    verifyHash(data, hash)
    {
        const decryptedHash = crypto.privateDecrypt(this._rsaMyPrivateKey, Buffer.from(hash, 'hex'));
        console.log('decrypted hash:', decryptedHash.toString('hex'));
        const calculatedHash = this.calculateHash(data);
        if (calculatedHash !== decryptedHash.toString('hex'))
        {
            throw new Error('Message was modified: Hash incorrect');
        }

        return decryptedHash.toString('hex');
    }

    verifySignature(data, signature)
    {
        const VERIFIER = crypto.createVerify('RSA-SHA256');
        VERIFIER.update(data);
        const IS_SIGNATURE_VALID = VERIFIER.verify(this._rsaOtherPublicKey, signature, 'hex');

        if (!IS_SIGNATURE_VALID)
        {
            throw new Error('Message was modified: Signature incorrect');
        }
    }

    decryptData(iv, data)
    {
        const decipher = crypto.createDecipheriv('aes-256-cbc', this._AES_KEY.toString('hex'), iv);
        let decryptedMessageValue = decipher.update(data, 'hex', 'utf8');
        decryptedMessageValue += decipher.final('utf8');
        return decryptedMessageValue.toString();
    }

    generateAllKeys()
    {
        const diffieHellmanPublicKey = this.generateDhKeys();
        let rsaMyPublicKey = this.generateRsaKeys();

        const GENERATED_KEYS = [diffieHellmanPublicKey, Buffer.from(rsaMyPublicKey.export({type: 'pkcs1', format: 'pem'}))];
        const GENERATED_KEYS_BUFFER = Buffer.concat(GENERATED_KEYS);

        // console.log(`---------------- Generated keys ----------------\n
        //     diffieHellmanPublicKey: \n ${diffieHellmanPublicKey.toString('hex')} \n
        //     rsaPublicKey: \n ${rsaMyPublicKey.export({type: 'pkcs1', format: 'pem'})} \n
        // `);

        return GENERATED_KEYS_BUFFER;
    }

    generateDhKeys()
    {
        this._diffieHellmanObject = crypto.createECDH('secp521r1');
        this._diffieHellmanObject.generateKeys();
        return this._diffieHellmanObject.getPublicKey();
    }

    generateRsaKeys()
    {
        const RSA_KEYS = crypto.generateKeyPairSync("rsa", {
            modulusLength: 2048,
        });

        this._rsaMyPrivateKey = RSA_KEYS.privateKey;
        return RSA_KEYS.publicKey;
    }

    saveOtherPublicKeys(keys_buffer)
    {
        const RECEIVED_KEYS = this.separateKeys(keys_buffer);
        this._rsaOtherPublicKey = RECEIVED_KEYS.rsaPublicKey;

        // console.log(`---------------- Saved keys ----------------\n
        //     diffieHellmanPublicKey: \n ${RECEIVED_KEYS.diffieHellmanPublicKey.toString('hex')} \n
        //     rsaPublicKey: \n ${RECEIVED_KEYS.rsaPublicKey} \n
        // `);

        const SECRET_KEY = this._diffieHellmanObject.computeSecret(RECEIVED_KEYS.diffieHellmanPublicKey);
        this._AES_KEY = crypto.createHash('sha256').update(SECRET_KEY).digest();

        this._AES_KEY = this._AES_KEY.subarray(0, this._AES_KEY.length / 2);
    }

    separateKeys(keys_buffer)
    {
        const RSA_PUBLIC_KEY = this.findPemKeyInString(keys_buffer.toString());
        const DIFFIE_HELLMAN_KEY_STOP_INDEX = keys_buffer.indexOf(RSA_PUBLIC_KEY);
        let diffieHellmanPublicKey = Buffer.alloc(DIFFIE_HELLMAN_KEY_STOP_INDEX);

        const _ = 0;
        const DIFFIE_HELLMAN_KEY_START_INDEX = 0;

        keys_buffer.copy(diffieHellmanPublicKey, _, DIFFIE_HELLMAN_KEY_START_INDEX, DIFFIE_HELLMAN_KEY_STOP_INDEX);

        return {
            rsaPublicKey: RSA_PUBLIC_KEY,
            diffieHellmanPublicKey: Buffer.from(diffieHellmanPublicKey)
        }
    }

    findPemKeyInString(input_string)
    {
        const pemRegex = /-----BEGIN [A-Z ]+-----\r?\n([A-Za-z0-9+/=\r\n]+)\r?\n-----END [A-Z ]+-----/g;
        const matches = input_string.match(pemRegex);

        if (matches && matches.length > 0)
        {
            return matches[0];
        }
        else
        {
            return null;
        }
    }
}

class ServerSecurity extends Security
{
    initNegotiation()
    {
        const KEYS_BUFFER = this.generateAllKeys();

        this._eventEmitter.emit("start_handshake", KEYS_BUFFER);
    }

    finishDiffieHellmanNegotiation(client_public_keys_buffer)
    {
        this.saveOtherPublicKeys(client_public_keys_buffer);
    }
}

class ClientSecurity extends Security
{
    confirmNegotiation(server_public_keys_buffer)
    {
        const GENERATED_KEYS_BUFFER = this.generateAllKeys();

        this._eventEmitter.emit("finish_handshake", GENERATED_KEYS_BUFFER);

        this.saveOtherPublicKeys(server_public_keys_buffer);
    }
}


module.exports =
{
    ServerSecurity,
    ClientSecurity
}