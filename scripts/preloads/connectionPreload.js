const {ipcRenderer} = require("electron");

window.addEventListener('DOMContentLoaded', () => {

    const connectButton = document.getElementById("connect");

    connectButton.addEventListener("click", ()=>{
        const address = document.getElementById("input-ip").value;
        ipcRenderer.send('connect', address);
    })
});