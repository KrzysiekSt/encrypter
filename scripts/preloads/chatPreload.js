const {ipcRenderer} = require("electron");

function createParagraph(text, align)
{
    const paragraph = document.createElement('p');
    paragraph.textContent = text;
    paragraph.style.textAlign = align;

    return paragraph;
}

window.addEventListener('DOMContentLoaded', () => {

    ipcRenderer.on('show_message', (event, message) => {
        const paragraph = createParagraph(message, "left");
        document.getElementById("message-box").appendChild(paragraph);
    });

    document.getElementById("send").addEventListener("click", function(event){
        let message = document.getElementById("input-message").value;
        const paragraph = createParagraph(message, "right");
        ipcRenderer.send('send_new_message', message);
        document.getElementById("message-box").appendChild(paragraph);
    })

});